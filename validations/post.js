const { check } = require('express-validator/check'),
      constants = require('../constants/constants');

const postSchema = [
    check('text')
    .exists().withMessage(constants.Validations.ShouldExist)
    .not().isEmpty().withMessage(constants.Validations.ShouldNotBeEmpty)
    .trim(),
];

module.exports = postSchema;
