const { check } = require('express-validator/check'),
      constants = require('../constants/constants');

const userSchema = [
    check('username')
    .exists().withMessage(constants.Validations.ShouldExist)
    .not().isEmpty().withMessage(constants.Validations.ShouldNotBeEmpty)
    .trim(),

    check('password')
    .exists().withMessage(constants.Validations.ShouldExist)
    .not().isEmpty().withMessage(constants.Validations.ShouldNotBeEmpty)
    .isLength({min: 6}).withMessage(constants.Validations.PasswordSixCharsLong)
    .trim()
];

module.exports = userSchema;
