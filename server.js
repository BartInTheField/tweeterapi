const express = require('express');
const mongoose = require('./config/mongoose');
const cors = require('cors');
const db = mongoose();
const app = express();
const http = require('http').createServer(app);
const jwt = require('express-jwt');
const config = require('./config/config');
const bodyParser = require('body-parser');

const authRoutes = require('./routes/authentication');
const postRoutes = require('./routes/post');

const corsOptions = {
    origin: process.env.ALLOW_ORIGIN || '*',
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true,
    methods: 'GET, POST, PUT, PATCH, DELETE, OPTIONS'
};


app.use(bodyParser.json({
    limit: '50mb'
}));
app.use('*', cors(corsOptions));

app.use(jwt({
    secret: config.jwtSecret
}).unless({
    path: [
        {
            url: '/api/v1/login',
            methods: ['POST']
        },
        {
            url: '/api/v1/register',
            methods: ['POST']
        },
        {
            url: '/api/v1/post',
            methods: ['GET']
        },
    ]
}));
app.use('/api/v1/', authRoutes);
app.use('/api/v1/', postRoutes);


app.use('*', (req, res) => {
    res.status(400).json({
        'error': 'This URL is not available.'
    });
});

const port = process.env.PORT || 4000;
http.listen(port, () => {
    console.log('running at port ' + port);
});
