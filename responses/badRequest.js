const HttpStatus = require('http-status-codes');

module.exports = (res, error, schemaValidation = false) => {
    return res.status(HttpStatus.BAD_REQUEST).json({
        errors: schemaValidation ? error.mapped() : error
    });
}