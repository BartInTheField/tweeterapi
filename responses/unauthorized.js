const HttpStatus = require('http-status-codes');

module.exports = (res, error) => {
    return res.status(HttpStatus.UNAUTHORIZED).json({
        errors: error
    });
}