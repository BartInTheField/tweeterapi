const express = require('express');
const routes = express.Router();
const config = require('../config/config');
const bcryptjs = require('bcryptjs');
const salt = 10;

const jwt = require('jsonwebtoken');

const userSchema = require('../validations/user');
const {
    validationResult
} = require('express-validator/check');

const UserModel = require('../models/user');

const BadRequest = require('../responses/badRequest');
const Unauthorized = require('../responses/unauthorized');


routes.post('/login', userSchema, async (req, res) => {
    console.log(req);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return BadRequest(res, errors, true);
    }

    const user = req.body;
    const userFound = await UserModel.findOne({
        username: user.username.toLowerCase()
    });

    if (!bcryptjs.compareSync(user.password, userFound? userFound.passwordHash : '')) {
        return Unauthorized(res, {
            message: 'Unable to login'
        });
    } else {
        res.json({
            token: jwt.sign({userId: userFound.id}, config.jwtSecret, {
                expiresIn: '12h'
            })
        });
    }
});

routes.post('/register', userSchema, async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return BadRequest(res, errors, true);
    }

    const newUserData = req.body;

    const userFound = await UserModel.findOne({
        username: newUserData.username.toLowerCase()
    });
    if (!userFound) {
        const newUser = new UserModel({
            username: newUserData.username.toLowerCase(),
            passwordHash: bcryptjs.hashSync(newUserData.password, salt)
        });
        await newUser.save();
        res.json({
            message: 'Registered ' + newUserData.username.toLowerCase()
        });
    } else {
        return BadRequest(res, {
            message: 'User already exist'
        });
    }

});

module.exports = routes;
