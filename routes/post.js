const express = require('express');
const routes = express.Router();
const config = require('../config/config');

const jwt = require('jsonwebtoken');

const postSchema = require('../validations/post');
const {
    validationResult
} = require('express-validator/check');

const PostModel = require('../models/post');

const BadRequest = require('../responses/badRequest');
const Unauthorized = require('../responses/unauthorized');


routes.post('/post', postSchema, (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return BadRequest(res, errors, true);
    }

    const token = req.headers.authorization.split(' ')[1];

    jwt.verify(token, config.jwtSecret, async (err, decoded) => {
        if (err) {
            return Unauthorized(res, {
                message: 'Can not make a post'
            });
        } else {
            const post = new PostModel({
                text: req.body.text,
                author: decoded.userId
            });
            await post.save();
            res.json({
                message: 'Post is made!'
            });
        }
    });
});

routes.get('/post', async (req, res) => {
    const posts = await PostModel.find()
        .populate({path: 'author', select: 'username'})
        .exec();
    res.json(posts.reverse());
});


module.exports = routes;
