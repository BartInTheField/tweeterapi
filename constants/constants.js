const ServerReply = {
  InternalServerError : 'Internal server error',
  NotAuthorized : 'Not authorized',
  NotFound : 'Not found',
  Conflict : 'Conflict',
  BadRequest : 'Bad request',
  InvalidCredentials : 'Invalid credentials',
  SavedSuccessfully : 'Saved successfully',
  NODIFFPASSWORD : 'NODIFFPASSWORD',
  INCORRECTPIN : 'INCORRECTPIN',
  PINEXPIRED : 'PINEXPIRED',
  CodeExpired : 'Code expired'
};

const Validations = {
  ShouldExist : 'should exist',
  ShouldNotBeEmpty : 'should not be empty',
  ShouldBeEmail : 'should be an email',
  ShouldBeANumber : 'should be numeric',
  PasswordSixCharsLong : 'passwords must be at least 6 chars long',
  CountryCodesTwoCharsLong : 'country codes must be max 2 chars long',
  ShouldBeSixCharsLong : 'should be 6 chars long',
  ShouldBePostalCodeFormat : 'should be a postalcode format',
  ShouldBePhoneNumberFormat : 'should be a phonenumber format',
  ShouldBeString : 'should be a string',
  ShouldBeValidDate : 'should be a valid date',
  ShouldBeAlpha : 'should not contain numbers',
  ShouldBeBit : 'should be 0 or 1',
  ShouldBeBoolean : 'should be true or false',
  ShouldBeValidType : 'should be a valid type',
  ShouldBeAValidFileType: 'should be a valid filetype',
  ShouldBe260CharLong: 'should be 260 chars long',
  ShouldBe2Long: 'should be 2 long',
  ShouldBe4Long: 'should be 4 long'
};

module.exports = {
  ServerReply,
  Validations
};
